package com.learnjava.custom.saga;

import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


/**
 * Saga steps composed with activity and corresponding compensation activity to invoke in case of Saga failure. 
 * 
 * @author muthu
 *
 * @param <R>
 * @param <I>
 */
@ToString
@Slf4j
public class SagaStep<Context extends Object>{
	
	@Setter
	private Object activityOutput;
	@Setter
	private Object compensationActivityOutput;
	@Setter
	private Exception activityException;
	private Activity<?,Context> activity;
	private Activity<?,Context> compensationActivity;
	private String stepId;
	
	/**
	 * Builds Saga Step. 
	 * 
	 * @return
	 */
	public static <Context> SagaStep<Context> build(){
		return new SagaStep<Context>();
	}
	
	/**
	 * Step Id. 
	 * 
	 * @param stepId
	 * @return
	 */
	SagaStep<Context> wtihStepId(String stepId) {
		this.stepId =  stepId;
		return this;
	}
	
	/**
	 * Activity will be a supplier of result after apply. 
	 * 
	 * @param activity
	 * @return
	 */
	@SuppressWarnings("unchecked")
	SagaStep<Context> wtihActivity(Activity<?, ?> activity) {
		this.activity =  (Activity<?, Context>) activity;
		return this;
	}
	
	/**
	 * Compensation activity will be a supplier of result after apply. 
	 * 
	 * @param compensationActivity
	 * @return
	 */
	@SuppressWarnings("unchecked")
	SagaStep<Context> wtihCompensationActivity(Activity<?, ?> compensationActivity) {
		this.compensationActivity =  (Activity<?, Context>) compensationActivity;
		return this;
	}
	
	/**
	 * No compensation activity if no action required. 
	 * 
	 * @return
	 */
	SagaStep<Context> wtihNoCompensationActivity() {
		compensationActivity =  new Activity<String, Context>() {
			@Override
			public String apply(String stepId, Context i) {
				log.info("No Compensation Activity Defined for Step : "+stepId);
				return null;
			}	
		};
		return this;
	}
	
	/**
	 * Executes the activity. 
	 * 
	 * @param i
	 */
	void executeActivity(Context context) {
		try {
			activityOutput = activity.apply(stepId, context);
		}
		catch(Exception exception) {
			this.activityException = exception;
			throw exception;
		}
		
	}
	
	/**
	 * Executes the compensation activity. 
	 * 
	 * @param i
	 */
	void executeCompensationActivity(Context context) {
		compensationActivityOutput = compensationActivity.apply(stepId, context);
	}

}
