package com.learnjava.custom.saga;

import java.util.LinkedList;

import lombok.Getter;


/**
 * A Saga is a sequence of steps. Each step performs the assigned activity to trigger the next step in the Saga. 
 * If a step fails, then the Saga executes a series of compensating activity that undo the changes, 
 * which were completed by the preceding steps.
 * 
 * @author muthu
 *
 * @param <Context>
 */
public class Saga<Context extends Object> {

	@Getter
	private LinkedList<SagaStep<Context>> steps = new LinkedList<SagaStep<Context>>();
	@Getter
	private boolean successStatus = false;
	
	/**
	 * Builds Saga. 
	 * 
	 * @param <Context>
	 * @return
	 */
	public static <Context> Saga<Context> saga() {
		return new Saga<Context>();
	}
	
	/**
	 * No Arg Constructor.
	 * 
	 */
	private Saga(){
		
	}
	
	/**
	 * Returns is the Saga is successful. 
	 * 
	 * @return
	 */
	public boolean isSuccessful() {
		return this.successStatus;
	}
	
	/**
	 * Adds Saga step. 
	 * 
	 * @param step
	 * @return
	 */
	public Saga<Context> add(SagaStep<Context> step){
		this.steps.add(step);
		return this;
	}
	
	/**
	 * Indicates end of Saga and returns the Saga reference. 
	 * 
	 * @param <Context>
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "hiding" })
	public <Context> Saga<Context> end(){
		return (Saga<Context>) this;
	}
	
	/**
	 * Starts the execution of Saga steps. 
	 * 
	 * @param context
	 */
	public void start(Context context) {
		int stepCount = 0;
		try {
			for(int i=0; i < steps.size(); i++) {
				stepCount = i;
				steps.get(stepCount).executeActivity(context);
			}
		}
		catch(Exception exception) {
			for(int i=(stepCount-1); i >= 0; i--) {
				steps.get(i).executeCompensationActivity(context);
			}
			this.successStatus = false;
			return;
		}
		this.successStatus = true;	
		return;
	}
		
}
