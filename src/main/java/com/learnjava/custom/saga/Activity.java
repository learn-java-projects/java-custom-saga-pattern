package com.learnjava.custom.saga;

/**
 * Activity will be a supplier of result after apply. 
 * 
 * 
 * @author muthu
 *
 * @param <R>
 * @param <I>
 */
@FunctionalInterface
public interface Activity<R extends Object, Context extends Object> {
	
	/**
	 * Applies and returns the result. 
	 * 
	 * @param stepId
	 * @param i
	 * @return
	 */
	R apply(String stepId, Context context);

}
