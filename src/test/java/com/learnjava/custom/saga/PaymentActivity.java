package com.learnjava.custom.saga;

import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PaymentActivity implements Activity<String, HashMap<String, Object>>{

	@Override
	public String apply(String stepId, HashMap<String, Object> context) {
		log.info("Payment Activity Step "+context.get("paymentId"));
		return null;
	}

}
