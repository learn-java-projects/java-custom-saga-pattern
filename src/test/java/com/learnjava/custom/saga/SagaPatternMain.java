package com.learnjava.custom.saga;

import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SagaPatternMain {


	public static void main(String args[]) {
		
		HashMap<String, Object> contextMap = new HashMap<String, Object>();
		contextMap.put("paymentId", "234235235");
			
		Saga<HashMap<String, Object>> sagaResult = Saga.saga()
														.add(SagaStep.build()
																.wtihStepId("Step #1")
																.wtihActivity((step, context) -> {
																	log.info("step 1 activity");
																	return "step 1 activity";
																})
																
																.wtihCompensationActivity((step, context) ->{
																	log.info("step 1 comp activity");
																	return "step 1 compensation result";
																})
															)
														.add(SagaStep.build()
																.wtihStepId("Step #2")
																.wtihActivity((step, context) -> {
																	log.info("step 2 activity");
																	return "step 2 activity";
																})
																.wtihNoCompensationActivity()
															)
														.add(SagaStep.build()
																.wtihStepId("Step #3")
																.wtihActivity((step, context) -> {
																	if(true) {
																		log.error("Error in step");
																		throw new RuntimeException("test");
																	}
																	log.info("step 3 activity");
																	return "step 3 activity";
																})
																.wtihNoCompensationActivity()
															)
														.end();
		
		sagaResult.start(contextMap);
		log.info("Successful : {}",sagaResult.isSuccessful());
		log.info("Result : {}", sagaResult.getSteps());
		
		
	}


}
